//
//  DeviceType.swift
//  ASH iOS Helpers
//
//  Created by Alexander Shmakov on 02.02.2018.
//  Copyright © 2018 Alexander Shmakov. All rights reserved.
//

import Foundation

public enum DeviceSize {
    
    /// 3.5'' (iPhone 4s and older)
    case inch3_5
    
    /// 4'' (iPhone 5, 5s, SE)
    case inch4
    
    /// 4.7'' (iPhone 6, 6s, 7, 8)
    case inch4_7
    
    /// 5.5'' (iPhone 6+, 7+, 8+)
    case inch5_5
    
    /// 5.8'' (iPhone X)
    case inch5_8
    
    /// 9.7'' (iPad mini, iPad Air, iPad Pro)
    case inch9_7
    
    /// 12.9'' (iPad Pro)
    case inch12_9
    
    init?() {
        let screenHeight = UIScreen.main.bounds.height
        let screenWidth  = UIScreen.main.bounds.width
        
        switch max(screenHeight, screenWidth) {
        case 480: self = .inch3_5
        case 568: self = .inch4
        case 667: self = .inch4_7
        case 736: self = .inch5_5
        case 812: self = .inch5_8
        case 1024: self = .inch9_7
        case 1366: self = .inch12_9
        default: return nil
        }
    }
}
