//
//  DeviceType.swift
//  ASH iOS Helpers
//
//  Created by Alexander Shmakov on 17/01/2019.
//  Copyright © 2019 Alexander Shmakov. All rights reserved.
//

import Foundation

public enum DeviceType {
    case iPodTouch5
    case iPodTouch6
    case iPhone4
    case iPhone4s
    case iPhone5
    case iPhone5c
    case iPhone5s
    case iPhone6
    case iPhone6Plus
    case iPhone6s
    case iPhone6sPlus
    case iPhone7
    case iPhone7Plus
    case iPhoneSE
    case iPhone8
    case iPhone8Plus
    case iPhoneX
    case iPhoneXS
    case iPhoneXSMax
    case iPhoneXR
    case iPad2
    case iPad3
    case iPad4
    case iPadAir
    case iPadAir2
    case iPad5
    case iPad6
    case iPadMini
    case iPadMini2
    case iPadMini3
    case iPadMini4
    case iPadPro97Inch
    case iPadPro129Inch
    case iPadPro129Inch2
    case iPadPro105Inch
    case appleTV
    case appleTV4K
    case homePod
    case simulator(model:String)
    case unknown
}
