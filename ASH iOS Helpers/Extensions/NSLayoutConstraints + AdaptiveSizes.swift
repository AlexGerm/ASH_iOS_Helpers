//
//  NSLayoutConstraints + Helpers.swift
//  ASH iOS Helpers
//
//  Created by Alexander Shmakov on 02.02.2018.
//  Copyright © 2018 Alexander Shmakov. All rights reserved.
//

import Foundation

/* Зависит от DeviceType */
extension NSLayoutConstraint {
    
    @IBInspectable var iPhone4 : CGFloat {
        get { return 0 }
        set {
            setConstant(value: newValue, deviceSize: .inch3_5)
        }
    }
    
    @IBInspectable var iPhoneSE : CGFloat {
        get { return 0 }
        set {
            setConstant(value: newValue, deviceSize: .inch4)
        }
    }
    
    @IBInspectable var iPhone8 : CGFloat {
        get { return 0 }
        set {
            setConstant(value: newValue, deviceSize: .inch4_7)
        }
    }
    
    @IBInspectable var iPhone8Plus : CGFloat {
        get { return 0 }
        set {
            setConstant(value: newValue, deviceSize: .inch5_5)
        }
    }
    
    @IBInspectable var iPhoneX : CGFloat {
        get { return 0 }
        set {
            setConstant(value: newValue, deviceSize: .inch5_8)
        }
    }
    
    @IBInspectable var iPadMini : CGFloat {
        get { return 0 }
        set {
            setConstant(value: newValue, deviceSize: .inch9_7)
        }
    }
    
    @IBInspectable var iPadPro : CGFloat {
        get { return 0 }
        set {
            setConstant(value: newValue, deviceSize: .inch12_9)
        }
    }
    
    
    private func setConstant(value : CGFloat, deviceSize : DeviceSize) {
        guard let currentDeviceType = DeviceSize() else {
            print("Could not determine the device!")
            return
        }
        if currentDeviceType == deviceSize {
            self.constant = value
        }
    }
    
}
