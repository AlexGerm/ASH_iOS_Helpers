//
//  UITableView + Registering.swift
//  ASH iOS Helpers
//
//  Created by Alexander Shmakov on 02.02.2018.
//  Copyright © 2018 Alexander Shmakov. All rights reserved.
//

import UIKit

public extension UITableView {
    
    public func register<T: UITableViewCell>(cell: T) {
        self.register(T.nib, forCellReuseIdentifier: String(describing: T.self))
    }
    
    public func register<T: UITableViewHeaderFooterView>(view: T) {
        self.register(T.nib, forHeaderFooterViewReuseIdentifier: String(describing: T.self))
    }

}

public extension UICollectionView {
    public func register<T: UICollectionViewCell>(cell: T) {
        self.register(T.nib, forCellWithReuseIdentifier: String(describing: T.self))
    }
}

