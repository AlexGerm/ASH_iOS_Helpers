//
//  Date + Helpers.swift
//  ASH iOS Helpers
//
//  Created by Alexander Shmakov on 02.02.2018.
//  Copyright © 2018 Alexander Shmakov. All rights reserved.
//

import Foundation

public extension Date {
    
    public static func fromString(string:String, format:String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: string)
    }
    
    public func toString (format:String, locale: String) -> String? {
        let formatter = DateFormatter()
        formatter.calendar = NSCalendar.current
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: locale)
        return formatter.string(from: self)
    }
    
}
