//
//  UIView + Helpers.swift
//  ASH iOS Helpers
//
//  Created by Alexander Shmakov on 02.02.2018.
//  Copyright © 2018 Alexander Shmakov. All rights reserved.
//

import Foundation

extension UIView {
    
    /// Shakes view. Useful to show invalid fields.
    public func shake(duration: TimeInterval = 0.6) {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = duration
        animation.values = [-10.0, 10.0, -8.0, 8.0, -6.0, 6.0, -4.0, 4.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    /// Make one or more corners of view round
    public func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.fillColor = UIColor.red.cgColor
        self.layer.mask = mask
        self.layer.masksToBounds = true
    }
    
    /// Loads instance from nib with the same name.
    public func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    /// Returns nib for class
    public static var nib: UINib {
        let nibName = String(describing: self)
        let bundle = Bundle(for: self)
        return UINib(nibName: nibName, bundle: bundle)
    }
}
