//
//  Array + Helpers.swift
//  ASH iOS Helpers
//
//  Created by Alexander Shmakov on 02.02.2018.
//  Copyright © 2018 Alexander Shmakov. All rights reserved.
//

import Foundation

public extension Array {
    
    var lastIndex: Int? {
        let index = self.count - 1
        return index >= 0 ? index : nil
    }
}
