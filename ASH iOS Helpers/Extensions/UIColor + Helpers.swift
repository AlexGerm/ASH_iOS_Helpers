//
//  UIColor + Helpers.swift
//  ASH iOS Helpers
//
//  Created by Alexander Shmakov on 02.02.2018.
//  Copyright © 2018 Alexander Shmakov. All rights reserved.
//

import Foundation

public extension UIColor {
    
    // Initialize color with rgb values
    public convenience init(red: Int, green: Int, blue: Int, alpha: CGFloat) {
        self.init(red: CGFloat(red) / 255, green: CGFloat(green) / 255, blue: CGFloat(blue) / 255, alpha: alpha)
    }
    
    /// Returns random color. Used for debugging ui-elements.
    public static func randomColor() -> UIColor {
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
    
}
