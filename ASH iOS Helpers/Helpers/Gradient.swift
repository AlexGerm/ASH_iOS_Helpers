//
//  Gradient.swift
//  ASH iOS Helpers
//
//  Created by Alexander Shmakov on 17/01/2019.
//  Copyright © 2019 Alexander Shmakov. All rights reserved.
//

import Foundation
import UIKit

/// Interface for gradient model
/// !Important Numbers of locations must be equal to colors number.
public protocol Gradient {
    var colors: [CGColor] { get }
    var locations: [NSNumber] { get }
    var direction: GradientDirection { get }
}

/// Basic implementation of Gradient protocol.
public struct DefaultGradient: Gradient {
    public let colors: [CGColor]
    public let locations: [NSNumber]
    public let direction: GradientDirection
}

/// Gradient direction
public enum GradientDirection {
    case topToBottom
    case leftToRight
    
    var startPoint: CGPoint {
        switch self {
        case .topToBottom:
            return CGPoint(x: 0.5, y: 0.0)
        case .leftToRight:
            return CGPoint(x: 0.0, y: 0.5)
        }
    }
    
    var endPoint: CGPoint {
        switch self {
        case .topToBottom:
            return CGPoint(x: 0.5, y: 1.0)
        case .leftToRight:
            return CGPoint(x: 1.0, y: 0.5)
        }
    }
}

public extension UIView {

    /// Applies gradient to view.
    /// !Important Number of locations in gradient must be equal to colors number, in other case it will generate locations automatically.
    public func applyGradient(gradient: Gradient) {
        let layer = CAGradientLayer()
        layer.colors = gradient.colors
        if !gradient.locations.isEmpty, gradient.locations.count == gradient.colors.count {
            layer.locations = gradient.locations
        }
        layer.startPoint = gradient.direction.startPoint
        layer.endPoint = gradient.direction.endPoint
        layer.frame = self.bounds
        self.layer.insertSublayer(layer, at: 0)
    }
}
