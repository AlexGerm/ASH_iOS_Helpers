//
//  ASH_iOS_HelpersTests.swift
//  ASH iOS HelpersTests
//
//  Created by Alexander Shmakov on 02.02.2018.
//  Copyright © 2018 Alexander Shmakov. All rights reserved.
//

import XCTest
@testable import ASH_iOS_Helpers

class ASH_iOS_HelpersTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testArrayLastIndex() {
        let array = [1, 2, 3]
        XCTAssert(array.lastIndex == 2)
    }
    
    func testEmptyArrayLastIndexReturnsNil() {
        let array = [Int]()
        XCTAssert(array.lastIndex == nil)
    }
}
